/////*
//// * To change this license header, choose License Headers in Project Properties.
//// * To change this template file, choose Tools | Templates
//// * and open the template in the editor.
//// */
package controller;


import com.dmssw.orm.models.MajlisMdCode;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;
import util.ResponseWrapper;

/**
 *
 * @author Sandali Kaushalya
 */
public class GroupControllerTest {

    public GroupControllerTest() {
    }

    @BeforeClass
    public static void setUpClass() {
    }

    @AfterClass
    public static void tearDownClass() {
    }

    @Before
    public void setUp() {
    }

    @After
    public void tearDown() {
    }
////
////    /**
////     * Test of getGroupStatus method, of class GroupController.
////     */
//////    @Test
//////    public void testGetGroupStatus() {
//////        System.out.println("getGroupStatus");
//////        GroupController instance = new GroupController();
//////        ResponseWrapper rw = new ResponseWrapper();
//////     //   rw.setResponseCode(null);
//////
//////        MajlisMdCode mdcode = new MajlisMdCode();
//////        mdcode.setCodeId(2);
//////        mdcode.setCodeSubType("ACTIVE_STATUS");
//////        mdcode.setCodeLocale("EN");
//////        mdcode.setCodeSeq(1);
//////        mdcode.setCodeMessage("Inactive");
//////        mdcode.setCodeType(null);
//////
//////        mdcode.setMajlisCategoryList(null);
//////        mdcode.setMajlisBirthdayTemplateList(null);
//////        mdcode.setMajlisNotificationList(null);
//////        mdcode.setMajlisGroupList(null);
//////        mdcode.setMajlisGroupList1(null);
//////        mdcode.setMajlisGroupEventList(null);
//////        mdcode.setMajlisCmsUsersList(null);
//////
//////        MajlisMdCode mdcode2 = new MajlisMdCode();
//////        mdcode2.setCodeId(3);
//////        mdcode2.setCodeSubType("ACTIVE_STATUS");
//////        mdcode2.setCodeLocale("EN");
//////        mdcode2.setCodeSeq(2);
//////        mdcode2.setCodeMessage("Active");
//////        mdcode.setCodeType(null);
//////     mdcode.setMajlisCategoryList(null);
//////        mdcode.setMajlisBirthdayTemplateList(null);
//////        mdcode.setMajlisNotificationList(null);
//////        mdcode.setMajlisGroupList(null);
//////        mdcode.setMajlisGroupList1(null);
//////        mdcode.setMajlisGroupEventList(null);
//////        mdcode.setMajlisCmsUsersList(null);
//////
//////        List<MajlisMdCode> mdCodeList = new ArrayList<>();
//////        mdCodeList.add(mdcode);
//////        mdCodeList.add(mdcode2);
//////
//////        rw.setResponseFlag(0);
//////        rw.setResponseData(mdCodeList);
//////
//////        ResponseWrapper expResult = rw;
//////
//////        ResponseWrapper result = instance.getGroupStatus();
//////        Object actuals = result.getResponseData();
//////
//////        assertFalse("testGetGroupStatus", !mdCodeList.equals(actuals));
//////
//////        //assertEquals(expResult, result);
//////        // assertEquals(expResult.getClass(), result.getClass());
//////        // TODO review the generated test code and remove the default call to fail.
//////        //fail("The test case is a prototype.");
//////    }
////
////    /**
////     * Test of getGroupPublicLevel method, of class GroupController.
////     */
    @Test
    public void testGetGroupPublicLevel() {
        System.out.println("getGroupPublicLevel");
        GroupController instance = new GroupController();
        ResponseWrapper rw = new ResponseWrapper();
//        
//
//        MajlisMdCode mdcode = new MajlisMdCode();
//        mdcode.setCodeId(5);
//        mdcode.setCodeSubType("GROUP_LEVELS");
//        mdcode.setCodeLocale("EN");
//        mdcode.setCodeSeq(1);
//        mdcode.setCodeMessage("None");
//        mdcode.setCodeType(null);
//   mdcode.setMajlisCategoryList(null);
//        mdcode.setMajlisBirthdayTemplateList(null);
//        mdcode.setMajlisNotificationList(null);
//        mdcode.setMajlisGroupList(null);
//        mdcode.setMajlisGroupList1(null);
//        mdcode.setMajlisGroupEventList(null);
//        mdcode.setMajlisCmsUsersList(null);
//
//        MajlisMdCode mdcode2 = new MajlisMdCode();
//        mdcode2.setCodeId(6);
//        mdcode2.setCodeSubType("GROUP_LEVELS");
//        mdcode2.setCodeLocale("EN");
//        mdcode2.setCodeSeq(2);
//        mdcode2.setCodeMessage("Country");
//        mdcode2.setCodeType(null);
//   mdcode.setMajlisCategoryList(null);
//        mdcode.setMajlisBirthdayTemplateList(null);
//        mdcode.setMajlisNotificationList(null);
//        mdcode.setMajlisGroupList(null);
//        mdcode.setMajlisGroupList1(null);
//        mdcode.setMajlisGroupEventList(null);
//        mdcode.setMajlisCmsUsersList(null);
//        MajlisMdCode mdcode3 = new MajlisMdCode();
//        mdcode3.setCodeId(7);
//        mdcode3.setCodeSubType("GROUP_LEVELS");
//        mdcode3.setCodeLocale("EN");
//        mdcode3.setCodeSeq(3);
//        mdcode3.setCodeMessage("District");
//        mdcode3.setCodeType(null);
//    mdcode.setMajlisCategoryList(null);
//        mdcode.setMajlisBirthdayTemplateList(null);
//        mdcode.setMajlisNotificationList(null);
//        mdcode.setMajlisGroupList(null);
//        mdcode.setMajlisGroupList1(null);
//        mdcode.setMajlisGroupEventList(null);
//        mdcode.setMajlisCmsUsersList(null);
//
//        MajlisMdCode mdcode4 = new MajlisMdCode();
//        mdcode4.setCodeId(8);
//        mdcode4.setCodeSubType("GROUP_LEVELS");
//        mdcode4.setCodeLocale("EN");
//        mdcode4.setCodeSeq(4);
//        mdcode4.setCodeMessage("City");
//        mdcode4.setCodeType(null);
//   mdcode.setMajlisCategoryList(null);
//        mdcode.setMajlisBirthdayTemplateList(null);
//        mdcode.setMajlisNotificationList(null);
//        mdcode.setMajlisGroupList(null);
//        mdcode.setMajlisGroupList1(null);
//        mdcode.setMajlisGroupEventList(null);
//        mdcode.setMajlisCmsUsersList(null);
//
//        List<MajlisMdCode> mdCodeList = new ArrayList<>();
//        mdCodeList.add(mdcode);
//        mdCodeList.add(mdcode2);
//        mdCodeList.add(mdcode3);
//        mdCodeList.add(mdcode4);
//
//        rw.setResponseFlag(1);
//        rw.setResponseData(mdCodeList);
//
//        ResponseWrapper expResult = rw;

        ResponseWrapper result = instance.getGroupPublicLevel();
//        Object actuals = result.getResponseData();
  
       assertFalse("testGetGroupPublicLevel", (result.getResponseFlag() == 9999));

        // TODO review the generated test code and remove the default call to fail.
//        fail("The test case is a prototype.");
    }

    /**
     * Test of CreateNewGroup method, of class GroupController.
     */
//    @Test
//    public void testCreateNewGroup() {
//        System.out.println("CreateNewGroup");
//        MajlisGroup mGroup = new MajlisGroup();
//        GroupController instance = new GroupController();
//
//        MajlisGroup group = new MajlisGroup();
//
//        Date today = new Date();
//
//        group.setDateInserted(today);
//        group.setDateModified(null);
//
//        MajlisCmsUsers admin = new MajlisCmsUsers();
//        admin.setUserFullName("admin");
//        admin.setUserId("1");
//
//        MajlisMdCode status = new MajlisMdCode();
//        status.setCodeId(3);
//        status.setCodeMessage("Active");
//        status.setCodeSeq(2);
//
//        //admin.setStatus(status);
//        group.setGroupAdmin(admin);
//        group.setGroupSystemId(20170540);
//        group.setGroupStatus(status);
//
//        MajlisMdCode level = new MajlisMdCode();
//        level.setCodeId(5);
//        level.setCodeMessage("None");
//        level.setCodeSeq(1);
//
//        group.setGroupPublicLevel(level);
//        group.setGroupTitle("groupTitle");
//
//        ResponseWrapper rw = new ResponseWrapper();
//        rw.setResponseFlag(1);
//
//        ResponseWrapper expResult = rw;
//
//        mGroup.setGroupPublicLevel(level);
//        mGroup.setGroupTitle("groupTitle");
//        mGroup.setGroupAdmin(admin);
//        mGroup.setGroupSystemId(20170540);
//        mGroup.setDateModified(null);
//        mGroup.setDateInserted(today);
//        mGroup.setGroupStatus(status);
//        ResponseWrapper result = instance.CreateNewGroup(mGroup);
//
//        System.out.println("check:" + result.getResponseFlag());
//        System.out.println("check expResult:" + expResult.getResponseFlag());
//        //System.out.println(".getResponseCode():" + result.getResponseCode());
//       // System.out.println(".getResponseCode() expResult:" + expResult.getResponseCode());
//        System.out.println(".getResponseData():" + result.getResponseData());
//        System.out.println(".getResponseData() expResult:" + expResult.getResponseData());
//        System.out.println(".getTotalRecords():" + result.getTotalRecords());
//        System.out.println(".getTotalRecords() expResult:" + expResult.getTotalRecords());
//        System.out.println("mGroup:" + mGroup.getGroupStatus());
//        System.out.println("group:" + group.getGroupStatus());
//
//        System.out.println("equals:" );
//        assertNotEquals(-1, result.getResponseFlag());
////        assertNotNull(result);
//        //assertFalse("testCreateNewGroup", rw.equals(result));
//        // TODO review the generated test code and remove the default call to fail.
//        //fail("The test case is a prototype.");
//    }
//
//    /**
//     * Test of getAllGroups method, of class GroupController.
//     */
//    @Test
//    public void testGetAllGroups() {
//        System.out.println("getAllGroups");
//        int userId = 1;
//        GroupController instance = new GroupController();
//
//        ResponseWrapper rw = new ResponseWrapper();
//        rw.setResponseFlag(1);
//        rw.setTotalRecords(4);
//        rw.setResponseData(null);
//
//        ResponseWrapper expResult = rw;
//        ResponseWrapper result = instance.getAllGroups(userId);
//
//        result.setResponseData(null);
//        System.out.println("equals:" + expResult.equals(result));
//        //assertEquals(expResult, result);
//
//        assertFalse("getAllGroups", rw.equals(result));

//        // TODO review the generated test code and remove the default call to fail.
////        fail("The test case is a prototype.");
//    }
//
//    /**
//     * Test of CreateNewGroup method, of class GroupController.
//     */
////    @Test
////    public void testCreateNewGroup() {
////        System.out.println("CreateNewGroup");
////        MajlisGroup mGroup = new MajlisGroup();
////        GroupController instance = new GroupController();
////
////        MajlisGroup group = new MajlisGroup();
////
////        Date today = new Date();
////
////        group.setDateInserted(today);
////        group.setDateModified(null);
////
////        MajlisCmsUsers admin = new MajlisCmsUsers();
////        admin.setUserFullName("admin");
////        admin.setUserId("1");
////
////        MajlisMdCode status = new MajlisMdCode();
////        status.setCodeId(3);
////        status.setCodeMessage("Active");
////        status.setCodeSeq(2);
////
////        //admin.setStatus(status);
////        group.setGroupAdmin(admin);
////        group.setGroupSystemId(20170540);
////        group.setGroupStatus(status);
////
////        MajlisMdCode level = new MajlisMdCode();
////        level.setCodeId(5);
////        level.setCodeMessage("None");
////        level.setCodeSeq(1);
////
////        group.setGroupPublicLevel(level);
////        group.setGroupTitle("groupTitle");
////
////        ResponseWrapper rw = new ResponseWrapper();
////        rw.setResponseFlag(1);
////
////        ResponseWrapper expResult = rw;
////
////        mGroup.setGroupPublicLevel(level);
////        mGroup.setGroupTitle("groupTitle");
////        mGroup.setGroupAdmin(admin);
////        mGroup.setGroupSystemId(20170540);
////        mGroup.setDateModified(null);
////        mGroup.setDateInserted(today);
////        mGroup.setGroupStatus(status);
////        ResponseWrapper result = instance.CreateNewGroup(mGroup);
////
////        System.out.println("check:" + result.getResponseFlag());
////        System.out.println("check expResult:" + expResult.getResponseFlag());
////        //System.out.println(".getResponseCode():" + result.getResponseCode());
////       // System.out.println(".getResponseCode() expResult:" + expResult.getResponseCode());
////        System.out.println(".getResponseData():" + result.getResponseData());
////        System.out.println(".getResponseData() expResult:" + expResult.getResponseData());
////        System.out.println(".getTotalRecords():" + result.getTotalRecords());
////        System.out.println(".getTotalRecords() expResult:" + expResult.getTotalRecords());
////        System.out.println("mGroup:" + mGroup.getGroupStatus());
////        System.out.println("group:" + group.getGroupStatus());
////
////        System.out.println("equals:" );
////        assertNotEquals(-1, result.getResponseFlag());
//////        assertNotNull(result);
////        //assertFalse("testCreateNewGroup", rw.equals(result));
////        // TODO review the generated test code and remove the default call to fail.
////        //fail("The test case is a prototype.");
////    }
////
////    /**
////     * Test of getAllGroups method, of class GroupController.
////     */
////    @Test
////    public void testGetAllGroups() {
////        System.out.println("getAllGroups");
////        int userId = 1;
////        GroupController instance = new GroupController();
////
////        ResponseWrapper rw = new ResponseWrapper();
////        rw.setResponseFlag(1);
////        rw.setTotalRecords(4);
////        rw.setResponseData(null);
////
////        ResponseWrapper expResult = rw;
////        ResponseWrapper result = instance.getAllGroups(userId);
////
////        result.setResponseData(null);
////        System.out.println("equals:" + expResult.equals(result));
////        //assertEquals(expResult, result);
////
////        assertFalse("getAllGroups", rw.equals(result));
////        // TODO review the generated test code and remove the default call to fail.
////        //fail("The test case is a prototype.");
////    }
////
////    /**
////     * Test of getGroupDetails method, of class GroupController.
////     */
////    @Test
////    public void testGetGroupDetails() {
////        System.out.println("getGroupDetails");
////        int groupId = 5;
////        GroupController instance = new GroupController();
////        ResponseWrapper rw = new ResponseWrapper();
////        rw.setResponseFlag(1);
////        rw.setResponseData(null);
////        
////        ResponseWrapper expResult = rw;
////        ResponseWrapper result = instance.getGroupDetails(groupId);
////        rw.setResponseFlag(1);
////        rw.setResponseData(null);
//////        assertEquals(expResult, result);
////        // TODO review the generated test code and remove the default call to fail.
////         assertFalse("getAllGroups", expResult.equals(result));
////        // fail("The test case is a prototype.");
////    }
////    /**
////     * Test of getAllGroups method, of class GroupController.
////     */
}
