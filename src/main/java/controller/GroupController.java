/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package controller;

import com.dmssw.orm.controllers.DbCon;
import com.dmssw.orm.controllers.ORMConnection;
import com.dmssw.orm.models.MajlisCategory;
import com.dmssw.orm.models.MajlisGroup;
import com.dmssw.orm.models.MajlisMdCode;
import config.AppParams;
import entities.EventGroup;
import java.awt.image.BufferedImage;
import java.io.ByteArrayInputStream;
import java.io.File;
import java.sql.Connection;
import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.Base64;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import javax.ejb.LocalBean;
import javax.ejb.Stateless;
import javax.ejb.TransactionManagement;
import javax.ejb.TransactionManagementType;
import javax.imageio.ImageIO;
import org.hibernate.Session;
import util.ResponseWrapper;

/**
 *
 * @author Sandali Kaushalya
 */
@Stateless
@LocalBean
@TransactionManagement(TransactionManagementType.BEAN)
public class GroupController {

    public ResponseWrapper CreateNewGroup(MajlisGroup mGroup) {
        System.out.println("CreateNewGroup method call.................");

        int groupId, sysId;

        Date today = new Date();
        mGroup.setDateInserted(today);

        ResponseWrapper rw = new ResponseWrapper();

        try {
            System.out.println("inner CreateNewGroup ");
            if ((mGroup.getGroupTitle() != null) && (mGroup.getGroupStatus() != null) && (mGroup.getGroupPublicLevel() != null)) {
                System.out.println("inside if condition...");
                ORMConnection conn = new ORMConnection();
                Session session = conn.beginTransaction();

                groupId = conn.createObject(session, mGroup);
                conn.commitObject(session);

                Calendar cal = Calendar.getInstance();
                cal.setTime(today);
                String year = String.valueOf(cal.get(Calendar.YEAR));
                String month = String.valueOf(cal.get(Calendar.MONTH) + 1);

                sysId = Integer.parseInt(year + month + groupId);
                System.out.println("system id = " + sysId);

                mGroup.setGroupSystemId(sysId);
                rw = ModifyGroup(mGroup);
                rw.setResponseFlag(groupId);

                System.out.println("end CreateNewGroup ");

            } else {
                rw.setResponseFlag(2); //data null
                rw.setResponseData("required details not provided.");
            }

        } catch (Exception e) {
            System.out.println("Error in CreateNewGroup method:  " + e.getMessage());
            rw.setResponseFlag(-1);
            rw.setResponseData("Error in CreateNewGroup method");
            e.printStackTrace();

        }

        return rw;
    }

    public ResponseWrapper ModifyGroup(MajlisGroup mGroup) {

        System.out.println("ModifyGroup method call.................");

        ResponseWrapper rw = new ResponseWrapper();

        ORMConnection conn = new ORMConnection();
        Session session = null;
        session = conn.beginTransaction();

        try {
            if (mGroup.getGroupId() != null) {
                System.out.println("mGroup.getGroupId()" + mGroup.getGroupId());

                //session = conn.beginTransaction();
                conn.updateObject(session, mGroup);
                rw.setResponseFlag(1);// successfull
                rw.setResponseData("successfull");
                rw.setTotalRecords(1);
            } else {
                rw.setResponseFlag(8); //No group id
                rw.setResponseData("No group id provided.");
                rw.setTotalRecords(0);
            }

        } catch (Exception e) {
            System.out.println("Error in ModifyGroup method:  " + e.getMessage());
            rw.setResponseFlag(-1);
            rw.setTotalRecords(0);
            rw.setResponseData("Error in ModifyGroup method");
            e.printStackTrace();

        }

        conn.commitObject(session);
        session.close();
        return rw;
    }

    public ResponseWrapper getGroupStatus() {
        System.out.println("getGroupStatus method call.................");

        Session session = null;
        List<MajlisMdCode> mdCodeList = new ArrayList<>();

        ResponseWrapper rw = new ResponseWrapper();
        int i = 0;

        try {
            ORMConnection conn = new ORMConnection();
            String selectQuery = "SELECT C FROM MajlisMdCode C WHERE C.codeType = 'GROUPS' AND C.codeSubType='ACTIVE_STATUS' AND C.codeLocale='" + AppParams.LOCALE + "'";

            List<Object> query = conn.hqlGetResults(selectQuery);
            for (Object object : query) {
                i++;
                MajlisMdCode mdCode = (MajlisMdCode) object;

                MajlisMdCode md = new MajlisMdCode();

                md.setCodeId(mdCode.getCodeId());
                md.setCodeLocale(mdCode.getCodeLocale());
                md.setCodeMessage(mdCode.getCodeMessage());
                md.setCodeSeq(mdCode.getCodeSeq());
                md.setCodeSubType(mdCode.getCodeSubType());

                mdCodeList.add(md);

                System.out.println("mcode out..... " + mdCode.getCodeMessage());

            }

            rw.setResponseData(mdCodeList);
            rw.setResponseFlag(1);
            rw.setTotalRecords(i);

        } catch (Exception e) {
            System.out.println("Error in getGroupStatus method:  " + e.getMessage());
            rw.setResponseFlag(-1);
            rw.setTotalRecords(i);
            rw.setResponseData("Error in getGroupStatus method");
            e.printStackTrace();
        }

        return rw;
    }

    public ResponseWrapper getGroupPublicLevel() {
        System.out.println("getGroupPublicLevel method call.................");

        Session session = null;
        List<MajlisMdCode> mdCodeList = new ArrayList<>();

        ResponseWrapper rw = new ResponseWrapper();
        int i = 0;

        try {
            ORMConnection conn = new ORMConnection();
            String SelectQuery = "SELECT C FROM MajlisMdCode C WHERE C.codeType = 'GROUPS' AND C.codeSubType='GROUP_LEVELS' AND C.codeLocale='" + AppParams.LOCALE + "'";
            List<Object> query = conn.hqlGetResults(SelectQuery);

            for (Object object : query) {
                i++;
                MajlisMdCode mdCode = (MajlisMdCode) object;

                MajlisMdCode md = new MajlisMdCode();

                md.setCodeId(mdCode.getCodeId());
                md.setCodeLocale(mdCode.getCodeLocale());
                md.setCodeMessage(mdCode.getCodeMessage());
                md.setCodeSeq(mdCode.getCodeSeq());
                md.setCodeSubType(mdCode.getCodeSubType());

                mdCodeList.add(md);
                rw.setResponseFlag(1);
                rw.setTotalRecords(i);
                System.out.println("mcode out..... " + mdCode.getCodeMessage());

            }

            rw.setResponseData(mdCodeList);

        } catch (Exception e) {

            rw.setResponseFlag(-1);
            rw.setTotalRecords(i);
            rw.setResponseData("Error in getGroupPublicLevel method");
            e.printStackTrace();
        }

        return rw;
    }

    public ResponseWrapper getAllGroups(int userId) {
        System.out.println("getAllGroups method call.................");

        ResponseWrapper rw = new ResponseWrapper();
        ORMConnection conn = new ORMConnection();

        MajlisGroup group = new MajlisGroup();
        List<MajlisGroup> groupList = new ArrayList<>();

        int i = 0;

        try {
            if (userId != 0) {

                if (userId == 1) {

                    System.out.println("equals to admin condition");

                    String selectQuery = "SELECT G FROM MajlisGroup G";
                    List<Object> li = conn.hqlGetResults(selectQuery);

                    for (Object obj : li) {
                        i++;
                        group = (MajlisGroup) obj;
                        groupList.add(group);

                    }

                } else {
                    String selectQuery = "SELECT G FROM MajlisGroup G "
                            + "WHERE G.groupAdmin = '" + userId + "'";

                    List<Object> list = conn.hqlGetResults(selectQuery);

                    for (Object obj : list) {
                        i++;
                        group = (MajlisGroup) obj;
                        groupList.add(group);

                    }
                }

                rw.setResponseData(groupList);
                rw.setResponseFlag(1);
                rw.setTotalRecords(i);

            } else {
                rw.setResponseFlag(8); // user id not passed
                rw.setResponseData("group admin id not provided.");
                rw.setTotalRecords(i);
            }
        } catch (Exception e) {
            System.out.println("Error in getAllGroups method:  " + e.getMessage());
            rw.setResponseFlag(-1); // exception occur
            rw.setTotalRecords(i);
            rw.setResponseData("Error in getAllGroups method");
            e.printStackTrace();
        }
        return rw;
    }

    public ResponseWrapper getGroupDetails(int groupId) {
        System.out.println("getGroupDetails method call.................");

        int i = 0;

        ResponseWrapper rw = new ResponseWrapper();
        ORMConnection conn = new ORMConnection();

        MajlisGroup group = new MajlisGroup();

        try {

            if (groupId != 0) {

                String selectQuery = "SELECT G FROM MajlisGroup G WHERE G.groupId='" + groupId + "'";
                List<Object> li = conn.hqlGetResults(selectQuery);

                for (Object obj : li) {
                    i++;
                    group = (MajlisGroup) obj;

                }
                rw.setResponseData(group);
                rw.setTotalRecords(i);
                rw.setResponseFlag(1);
                rw.setResponseData("successfull");//success

            } else {
                rw.setResponseFlag(8); // group id not provided
                rw.setTotalRecords(i);
                rw.setResponseData("Group id not provided.");
            }

        } catch (Exception e) {
            System.out.println("Error in getGroupDetails method:  " + e.getMessage());
            rw.setResponseFlag(-1); // exception occur
            rw.setTotalRecords(i);
            rw.setResponseData("Error in getAllGroups method");
            e.printStackTrace();
        }
        return rw;
    }

    public ResponseWrapper ValidateGroupIcon(MajlisGroup mGroup) {

        String base64Img = mGroup.getGroupIconPath();
        int groupId = mGroup.getGroupId();

        ResponseWrapper rw = new ResponseWrapper();
        System.out.println(base64Img);

        if ((base64Img != null) && (base64Img.length() > 0)) {
            try {

                byte[] decodedBytes = Base64.getDecoder().decode(base64Img);

                final BufferedImage bufferedImage = ImageIO.read(new ByteArrayInputStream(decodedBytes));

                String image_URL = AppParams.UPLOAD_DIRECTORY + "/" + groupId + "_GroupIcons" + ".jpg";
                System.out.println("Stack Graphic Image Path: " + image_URL);
                try {
                    File f = new File(image_URL);
                    ImageIO.write(bufferedImage, "PNG", f);
                    mGroup.setGroupIconPath(image_URL);

                    rw.setResponseData("successfull");
                    rw.setResponseFlag(1);
                    rw.setTotalRecords(1);

                } catch (Exception e) {
                    e.printStackTrace();
                    System.out.println("Error Saving Image : " + e.getMessage());

                    rw.setResponseData("Error Saving Image");
                    rw.setResponseFlag(-1);
                    rw.setTotalRecords(0);
                }
            } catch (Exception ex) {
                System.out.println("Error in ValidateGroupIcon: " + ex.getMessage());
                rw.setResponseData("Error in ValidateGroupIcon");
                rw.setResponseFlag(-1);
                rw.setTotalRecords(0);

            }

        }

        return rw;
    }

    public ResponseData getAllGroupDetails(int groupId, String groupTitle, String groupCountry, String groupDistrict, String groupCity) {

        System.out.println("getAllGroupDetails method call.................");
        ResponseData rd = new ResponseData();
        DbCon db = new DbCon();

        Connection conn = db.getCon();
        List<EventGroup> groupList = new ArrayList<>();
        try {
            String where = (groupId == 0 ? " g.GROUP_ID= g.GROUP_ID" : " g.GROUP_ID = '" + groupId + "'");
            where += (groupTitle.toUpperCase().equals("ALL")) ? " AND IFNULL(g.GROUP_TITLE,'AA') =IFNULL(g.GROUP_TITLE,'AA') " : " AND g.GROUP_TITLE LIKE '%" + groupTitle + "%' ";
            where += (groupCountry.toUpperCase().equals("ALL")) ? " AND IFNULL(g.GROUP_COUNTRY,'AA')=IFNULL(g.GROUP_COUNTRY,'AA') " : " AND g.GROUP_COUNTRY LIKE '%" + groupCountry + "%' ";
            where += (groupDistrict.toUpperCase().equals("ALL")) ? " AND IFNULL(g.GROUP_DISTRICT,'AA') = IFNULL(g.GROUP_DISTRICT,'AA') " : " AND g.GROUP_DISTRICT LIKE '%" + groupDistrict + "%' ";
            where += (groupCity.toUpperCase().equals("ALL")) ? " AND IFNULL(g.GROUP_CITY,'AA')=IFNULL(g.GROUP_CITY,'AA') " : " AND g.GROUP_CITY LIKE '%" + groupCity + "%' ";

            String sql = "SELECT * "
                    + "FROM majlis_group g WHERE " + where;

            System.out.println("query... " + sql);

            ResultSet rs = db.search(conn, sql);

            while (rs.next()) {

                EventGroup m = new EventGroup();

                m.setGroupId(rs.getString("GROUP_ID"));
                m.setTitle(rs.getString("GROUP_TITLE"));
                m.setCountry(rs.getString("GROUP_COUNTRY"));
                m.setDistrict(rs.getString("GROUP_DISTRICT"));
                m.setCity(rs.getString("GROUP_CITY"));

                groupList.add(m);

            }
            List<MajlisGroup> privateGroupList = getAllPrivateGroupDetails();
            if( privateGroupList != null){
                
                rd.setResponseCode( privateGroupList);
            }
            else{
                rd.setResponseCode("No Private groups available.");
            }
        
            rd.setResponseData(groupList);

            conn.close();
        } catch (Exception e) {
            System.out.println("Error in ValidateGroupIcon: " + e.getMessage());
            e.printStackTrace();
        }

        return rd;
    }

    public List<MajlisGroup> getAllPrivateGroupDetails() {
        System.out.println("getAllPrivateGroupDetails method call.................");

        Session session = null;
        int code =0;
      
        ORMConnection conn = new ORMConnection();
        
        List<MajlisGroup> privateGroupList = new ArrayList<>();

        try {
            String selectMdCode = "SELECT m.codeId FROM MajlisMdCode m WHERE m.codeMessage= 'None'";
            List<Object> li = conn.hqlGetResults(selectMdCode);

            for (Object obj : li) {
                code = (int) obj;
                System.out.println("code"+code);

            }
            
            if(code !=0){
                System.out.println("not = 0");            
                String selectPrivateGroups = "SELECT g FROM MajlisGroup g WHERE g.groupPublicLevel= '"+code+"'";
                List<Object> privateList = conn.hqlGetResults(selectPrivateGroups);
                
                for (Object object : privateList) {
                    MajlisGroup pGroup = (MajlisGroup) object;
                    privateGroupList.add(pGroup);
                }
               
            }
            
            else {                
               privateGroupList = null;
            }
           
        }

        catch (Exception e) {
            System.out.println("Error in getAllPrivateGroupDetails: " + e.getMessage());
             privateGroupList = null;
            e.printStackTrace();
          
        }
        return privateGroupList;
    }

}
