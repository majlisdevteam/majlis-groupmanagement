/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package util;

/**
 *
 * @author Sandali Kaushalya
 */
public class ResponseWrapper {

  
    private int responseFlag;
    private int totalRecords;
    private Object responseData;
    
    

    public int getTotalRecords() {
        return totalRecords;
    }

    public void setTotalRecords(int totalRecords) {
        this.totalRecords = totalRecords;
    }



    public Object getResponseData() {
        return responseData;
    }

    public void setResponseData(Object responseData) {
        this.responseData = responseData;
    }

    /**
     * @return the responseFlag
     */
    public int getResponseFlag() {
        return responseFlag;
    }

    /**
     * @param responseFlag the responseFlag to set
     */
    public void setResponseFlag(int responseFlag) {
        this.responseFlag = responseFlag;
    }

}
