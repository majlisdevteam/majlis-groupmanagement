/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package util;

import java.util.List;

/**
 *
 * @version 1.0 / 15 March 2017
 * @author Sandali Kaushalya
 */
public class ResponseData {

    private int responseCode;
    private int responseFlag;
    private String passCode;
    private String responseDetails;
    private int sequenceId;
    
    private List<?> responseData;

    /**
     * @return the responseCode
     */
    public int getResponseCode() {
        return responseCode;
    }

    /**
     * @param responseCode the responseCode to set
     */
    public void setResponseCode(int responseCode) {
        this.responseCode = responseCode;
    }

    /**
     * @return the responseFlag
     */
    public int getResponseFlag() {
        return responseFlag;
    }

    /**
     * @param responseFlag the responseFlag to set
     */
    public void setResponseFlag(int responseFlag) {
        this.responseFlag = responseFlag;
    }

    /**
     * @return the passCode
     */
    public String getPassCode() {
        return passCode;
    }

    /**
     * @param passCode the passCode to set
     */
    public void setPassCode(String passCode) {
        this.passCode = passCode;
    }

    /**
     * @return the responseDetails
     */
    public String getResponseDetails() {
        return responseDetails;
    }

    /**
     * @param responseDetails the responseDetails to set
     */
    public void setResponseDetails(String responseDetails) {
        this.responseDetails = responseDetails;
    }

    /**
     * @return the sequenceId
     */
    public int getSequenceId() {
        return sequenceId;
    }

    /**
     * @param sequenceId the sequenceId to set
     */
    public void setSequenceId(int sequenceId) {
        this.sequenceId = sequenceId;
    }

    /**
     * @return the responseData
     */
    public List<?> getResponseData() {
        return responseData;
    }

    /**
     * @param responseData the responseData to set
     */
    public void setResponseData(List<?> responseData) {
        this.responseData = responseData;
    }

    


    
     
}

